﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    Empty,
    Outside,    // outside
    Outside1,   // 1 is outside (12)
    Outside2,   // 2 is outside (02)
    Outside3,   // 3 is outside (01)
    Inside1,    // 1 is inside  (0)
    Inside2,    // 2 is inside  (1)
    Inside3,    // 3 is inside  (2)
    Inside      // inside
}

public class Tile : MonoBehaviour
{
    public TileType tileType;

    public int x;
    public int z;

    public Selector[] verticesSelectors = { null, null, null };

    [SerializeField]
    private GameObject EmptyTilePrefab = null;
    [SerializeField]
    private GameObject OutsideTilePrefab = null;
    [SerializeField]
    private GameObject Outside1TilePrefab = null;
    [SerializeField]
    private GameObject Outside2TilePrefab = null;
    [SerializeField]
    private GameObject Outside3TilePrefab = null;
    [SerializeField]
    private GameObject InsideTilePrefab = null;
    [SerializeField]
    private GameObject Inside1TilePrefab = null;
    [SerializeField]
    private GameObject Inside2TilePrefab = null;
    [SerializeField]
    private GameObject Inside3TilePrefab = null;

    public void bindVerticesSelectors(Selector[] newSelectors)
    {
        if(verticesSelectors[0]  != null) verticesSelectors[0].ClickEvent -= CallUpdateTile;
        if(verticesSelectors[1]  != null) verticesSelectors[1].ClickEvent -= CallUpdateTile;
        if(verticesSelectors[2]  != null) verticesSelectors[2].ClickEvent -= CallUpdateTile;

        verticesSelectors[0] = newSelectors[0];
        verticesSelectors[1] = newSelectors[1];
        verticesSelectors[2] = newSelectors[2];
        
        if(verticesSelectors[0]  != null) verticesSelectors[0].ClickEvent += CallUpdateTile;
        if(verticesSelectors[1]  != null) verticesSelectors[1].ClickEvent += CallUpdateTile;
        if(verticesSelectors[2]  != null) verticesSelectors[2].ClickEvent += CallUpdateTile;
    }

    public void CallUpdateTile(object sender, SelectorEventArgs e)
    {
        UpdateTile();
    }

    public void UpdateTile()
    {
        if(verticesSelectors[0] != null && verticesSelectors[0].Selected)
        {
            if (verticesSelectors[1] != null && verticesSelectors[1].Selected)
            {
                if (verticesSelectors[2] != null && verticesSelectors[2].Selected)
                {
                    LoadTile(TileType.Inside);
                }
                else
                {
                    //01
                    LoadTile(TileType.Outside3);
                }
            }
            else
            {
                if (verticesSelectors[2] != null && verticesSelectors[2].Selected)
                {
                    //02
                    LoadTile(TileType.Outside2);
                }
                else
                {
                    //0
                    LoadTile(TileType.Inside1);
                }
            }
        }
        else
        {
            if (verticesSelectors[1] != null && verticesSelectors[1].Selected)
            {

                if (verticesSelectors[2] != null && verticesSelectors[2].Selected)
                {
                    //12
                    LoadTile(TileType.Outside1);
                }
                else
                {
                    //1
                    LoadTile(TileType.Inside2);
                }
            }
            else
            {

                if (verticesSelectors[2] != null && verticesSelectors[2].Selected)
                {
                    //2
                    LoadTile(TileType.Inside3);
                }
                else
                {
                    if (verticesSelectors[0] == null &&
                        verticesSelectors[1] == null &&
                        verticesSelectors[2] == null)
                    {
                        LoadTile(TileType.Empty);
                    }
                    else
                    {
                        LoadTile(TileType.Outside);
                    }
                }
            }
        }
    }

    public void LoadTile(TileType newTileType)
    {
        if (tileType == newTileType && transform.childCount != 0)
            return;

        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        tileType = newTileType;

        switch (tileType)
        {
            case TileType.Empty:
                Object.Instantiate(EmptyTilePrefab, transform);
                break;
            case TileType.Outside:
                Object.Instantiate(OutsideTilePrefab, transform);
                break;
            case TileType.Outside1:
                Object.Instantiate(Outside1TilePrefab, transform);
                break;
            case TileType.Outside2:
                Object.Instantiate(Outside2TilePrefab, transform);
                break;
            case TileType.Outside3:
                Object.Instantiate(Outside3TilePrefab, transform);
                break;
            case TileType.Inside:
                Object.Instantiate(InsideTilePrefab, transform);
                break;
            case TileType.Inside1:
                Object.Instantiate(Inside1TilePrefab, transform);
                break;
            case TileType.Inside2:
                Object.Instantiate(Inside2TilePrefab, transform);
                break;
            case TileType.Inside3:
                Object.Instantiate(Inside3TilePrefab, transform);
                break;
        }
    }
}
