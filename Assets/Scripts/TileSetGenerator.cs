﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSetGenerator : MonoBehaviour
{
    private List<List<Tile>> tiles = new List<List<Tile>>();
    private List<List<Selector>> selectors = new List<List<Selector>>();
    
    [SerializeField]
    private Tile DefaultTilePrefab = null;

    [SerializeField]
    private Selector SelectorPrefab = null;

    [SerializeField]
    private float tileWidth = 0.875f;

    [SerializeField]
    private float tileLength = 1.5f;

    [SerializeField]
    private int tileSetWidth = 10;

    [SerializeField]
    private int tileSetLength = 10;

    void Start()
    {
        for (int i = 0; i < tileSetWidth; ++i)
        {
            tiles.Add(new List<Tile>());

            if (i < tileSetWidth - 2)
            {
                selectors.Add(new List<Selector>());
            }

            for (int j = 0; j < tileSetLength; ++j)
            {
                tiles[i].Add(Object.Instantiate(DefaultTilePrefab,
                    new Vector3(i * tileWidth, 0, j * tileLength + ((((i + j) % 2) == 0) ? 0.5f : 0)),
                    (((i + j) % 2) == 0) ? Quaternion.Euler(90, 180, 0) : Quaternion.Euler(90, 0, 0),
                    transform));

                tiles[i][j].x = i;
                tiles[i][j].z = j;

                if ((i < tileSetWidth - 2) && (j < tileSetLength - 1)) {
                    if ((i + j) % 2 == 0)
                    {
                        selectors[i].Add(Object.Instantiate(SelectorPrefab,
                            new Vector3((i + 1) * tileWidth, 0.5f, j * tileLength + 1),
                            Quaternion.identity,
                            transform));

                        selectors[i][j].x = i;
                        selectors[i][j].z = j;
                    }
                    else
                    {
                        selectors[i].Add(null);
                    }
                }

                Selector[] verticesSelectors = { null, null, null };

                if ((i + j) % 2 == 0)
                {
                    if ((i < tileSetWidth - 2) && (j < tileSetLength - 1))
                        verticesSelectors[0] = selectors[i][j];

                    if (i > 1)
                    {
                        if(j < tileSetLength - 1)
                            verticesSelectors[2] = selectors[i - 2][j];
                    }

                    if ((i > 0) && (j > 0) && (i < tileSetWidth - 1))
                    {
                        verticesSelectors[1] = selectors[i - 1][j - 1];
                    }
                }
                else
                {
                    if (i > 0)
                    {
                        if ((i < tileSetWidth - 1) && (j < tileSetLength - 1))
                            verticesSelectors[1] = selectors[i - 1][j];
                    }

                    if ((i > 1) && (j > 0))
                        verticesSelectors[0] = selectors[i - 2][j - 1];

                    if ((j > 0) && (i < tileSetWidth - 2))
                            verticesSelectors[2] = selectors[i][j - 1];
                }

                tiles[i][j].bindVerticesSelectors(verticesSelectors);
                tiles[i][j].UpdateTile();
            }
        }
    }
}
