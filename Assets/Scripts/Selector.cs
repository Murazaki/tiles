﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorEventArgs
{
    public int x { get; } // readonly
    public int z { get; } // readonly
    public int selected { get; } // readonly

    public SelectorEventArgs(int x , int z, bool selected) { x = x; z = z; selected = selected; }
}

[RequireComponent(typeof(MeshRenderer))]
public class Selector : MonoBehaviour
{
    MeshRenderer m_Renderer;

    [SerializeField]
    Color m_IdleColor = Color.red;

    [SerializeField]
    Color m_MouseOverColor = Color.blue;

    [SerializeField]
    Color m_MouseClickColor = Color.blue;

    [SerializeField]
    bool m_Selected = false;

    public int x;
    public int z;

    public bool Selected => m_Selected;

    public delegate void ClickEventHandler(object sender, SelectorEventArgs e);

    public event ClickEventHandler ClickEvent;

    void Start()
    {
        m_Renderer = GetComponent<MeshRenderer>();

        m_Renderer.material.color = m_IdleColor;
    }

    void OnMouseOver()
    {
        m_Renderer.material.color = m_MouseOverColor;
    }

    void OnMouseExit()
    {
        m_Renderer.material.color = m_IdleColor;
    }

    void OnMouseDown()
    {
        m_Selected = !m_Selected;

        m_Renderer.material.color = m_MouseClickColor;

        ClickEvent?.Invoke(this, new SelectorEventArgs(this.x, this.z, this.Selected));
    }

    void OnMouseUpAsButton()
    {
        m_Renderer.material.color = m_MouseOverColor;
    }
}
